import cv2
import os
from time import sleep

# taking user name to create a new directory for them
username=input("Enter Name of user :")
# checking if directory exists
# os.path('images')
try:
    # if exists okay or create one
    if not os.path.exists('images'):
        # using makedirs will create exact subdirectory list if not exists
        os.makedirs('images')
except OSError:
    print("Creating failed")
# creating a new user directory
os.makedirs(f'images/{username}')
# print(userdirectory)
# if not default/username
i=0
# capturing the data from camera
# starting camera to capture
cap=cv2.VideoCapture(0)
while(True):
    # initializing two values of images 
    ret,frame =cap.read()
    # showing the window of capture to user
    cv2.imshow("capturing",frame)
    # creating a name variable and storing it in a directory
    name = f"./images/{username}/{username}{i}.jpg"
    # writing the image to location
    cv2.imwrite(name,frame)
    # pausing the code for 2 seconds
    sleep(1)
    # showing log in terminal
    print(f'captured image:{i}')
    i+=1
    # to close the loop 
    key=cv2.waitKey(1)
    # to break loop after capturing certain images
    if i==30:
        break
    # exit if pressed q
    if key==ord('q'):
        break

cap.release()
cv2.destroyAllWindows()

import cv2
import numpy
import matplotlib.pyplot as plt 

cap = cv2.VideoCapture(0)
img_capture=0
while (cap.isOpened()):
    # storing the captured data after converting its color to rgb
    # image = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
    ret,frame=cap.read()
    image=frame
    # showing the captured image 
    cv2.imshow("capturing",image)
    # waiting for user input to exit
    # if cap.isOpened():
    #     img_name= f'image{img_capture}.png'
    #     cv2.imwrite(img_name,image)
    key=cv2.waitKey(1)
        img_capture=img_capture+1
    if key == ord('q'):
        break
    break
# shutdown camera
cap.release()
# close the window
cv2.destroyAllWindows()

